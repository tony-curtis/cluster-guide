\documentclass[10pt,english]{article}
\usepackage[T1]{fontenc}
% \usepackage[latin1]{inputenc}
\usepackage{geometry}
\geometry{verbose,tmargin=2cm,bmargin=2cm,lmargin=3cm,rmargin=3cm}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{babel}
\usepackage{fancybox}
% \usepackage{calc}
\usepackage[unicode=true]{hyperref}
% \usepackage{breakurl}
\usepackage{graphicx}

\usepackage[parfill]{parskip}

\usepackage{hyperref}

\usepackage[usenames,dvipsnames]{color}
\definecolor{ListingBG}{rgb}{0.91,0.91,0.91}
% \usepackage{courier}
\usepackage{listings}
\lstset{
  basicstyle=\scriptsize\ttfamily,
  backgroundcolor=\color{ListingBG},
  frame=tlBR
}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\usepackage{array}

\newcommand{\splashysays}[1] {
  \begin{table}[h]
    \begin{center}
    \Ovalbox{
      \begin{tabular}{p{0.1\linewidth} b{0.5\linewidth}}
        \includegraphics[scale=0.15]{./whale.png} & {\small \textbf{Splashy the Whale says:} #1} \\
      \end{tabular}
    }
    \end{center}
  \end{table}
}

% ----------------------------------------------------------------------

\begin{document}

\begin{titlepage}
  \begin{center}

    \vspace{1.0in} ~ \\

    \HRule \\[0.1in]
    {\LARGE \textsc{Quick Guide to the Crill and Whale Clusters}} \\
    \HRule \\[0.5in]

    \vspace{1.0in}

    \begin{table}[h]
      \begin{center}
        \begin{tabular}{l c r}
          Tony Curtis <tonyc@uh.edu> & \& & Dr. Edgar Gabriel <gabriel@cs.uh.edu> \\
          \multicolumn{3}{c}{\small \textit{(\href{http://www.imdb.com/title/tt0066701/?ref_=fn_al_tt_4}{The Persuaders!})}} \\
        \end{tabular}
      \end{center}
    \end{table}

    \vspace{1.0in}
    
    \includegraphics[width=0.5\textwidth]{./whale.png}

    \begin{table}[h]
      \begin{center}
        \begin{tabular}{l c r}
          \multicolumn{3}{c}{and Splashy the Whale} \\
          \\
          \\
          \multicolumn{3}{c}{\today{}}
        \end{tabular}
      \end{center}
    \end{table}

  \end{center}
\end{titlepage}

\tableofcontents

\newpage

\section{Crill \& Whale: What's what?}

The clusters consist of a login node (\texttt{crill.cs.uh.edu} and
\texttt{whale.cs.uh.edu} resp.) and a number of compute nodes that are
configured via units called ``partitions''; e.g.\ in the crill
cluster, the 48-core Opteron nodes are part of the partition also
called ``crill'', and all the 8-core whale nodes are in the ``calc''
partition (both of these are the per-cluster default partition).

The clusters are managed separately: jobs and nodes on one do not mix
with jobs and nodes on the other.  However, the clusters do share home
directories.

%% \splashysays{If you've been at UH for a while, you will recall a ``shark''
%%   cluster: this is a set of nodes that is now really old and has been
%%   repurposed for some experimental work}

Both clusters have Infiniband interconnects throughout, Gigabit
Ethernet management networks, and run 64-bit OpenSUSE Linux.

Some of the nodes in the crill cluster have QDR Infiniband cards.

\splashysays{Note that the OpenSUSE release may not be the same on
  both clusters: sometimes they drift due to supporting different
  software and hardware needs, and due to upgrade cycles.  Sysadmin is
  done by the authors on a volunteer and part-time basis.}

The examples below all refer to the crill cluster, but whale usage is
the same, since they run the same brand of job scheduler,
\hyperref[sec:slurm]{SLURM}.

\subsection{Who Has Access To The Clusters?}

The clusters are owned by Dr. Chapman's and Dr. Gabriel's research
groups.  However, some other people may also have access at various
times, e.g.\ external collaborating researchers and classes.
Partitions will be used to prevent too many resource collisions.

Since we're in the business of doing very weird, unpredictable things
for programming models research, the clusters are configured in a very
loose manner; production HPC systems are much more tightly controlled
with strict job time-limits and other constraints.

\splashysays{It is \emph{your} responsibility to check \emph{your} usage
  of the system and not keep hold of resources you no longer need.
  Splashy will zap allocations left idle for a long time.}

\subsection{Logging In}

Access to the clusters is through Secure Shell.  So you will require
an SSH client on the machine from which you are connecting.  The
client must support SSH protocol 2 (which is standard these days).
Client/server X11 forwarding is enabled both on the login nodes and on
the compute nodes.

\subsection{Login Node Usage}

The login nodes are to be used for editing, compiling and similar
activities.  They are \emph{not} to be used for running jobs such as
parallel programs.  Program runs are submitted through the SLURM
scheduler, which is discussed below.

\splashysays{big parallel compiles, e.g.\ with \texttt{make -j N} should
  be done on a compute node in a job allocation, see section
  \ref{sec:interactive}}

\subsection{Changing Password}

Your login password is changed with the usual \texttt{passwd} command.
The password \emph{must} be changed on the cluster's login node, not
on a compute node.  You can set up SSH keys to automate logins.

\splashysays{If you change your password on a compute node, the change
  will \emph{only} affect that compute node, \emph{not} the login
  node}

% \vspace{0.1in}
% \begin{minipage}{\linewidth}
% \includegraphics[scale=0.35]{gpubox.png}
% \end{minipage}

\section{What is SLURM?}
\label{sec:slurm}

SLURM is the ``Simple Linux Utility for Resource Management''.  It is
a job scheduler that's easy to use, but omits some of the features of
more sophisticated schedulers (e.g.\ Grid Engine, Torque, LSF, Moab).

Instead of running programs directly on the cluster login node, jobs
are instead submitted to SLURM, which then allocates resources to the
job.  The job is essentially a box that contains a request for
resources and a program to run using those resources.  Requests can
vary from very general (e.g.\ ``give me 4 cores from anywhere'') to
very specific (e.g.\ ``give me 2 nodes exclusively with at least 24
x86\_64 cores and each with at least 1 GPGPU''), depending on what you
want to do.  The basic idea is to request ``capabilities'' rather than
specific nodes, so that the resources can be allocated as needed.

\subsection{System Query}

The current state of SLURM can be queried via 3 basic commands.

\vspace{0.1in}
\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    Command      & What it does                               \tabularnewline
    \hline
    \hline
    sinfo        & state of partitions                        \tabularnewline
    \hline
    squeue       & what jobs are running and/or waiting now   \tabularnewline
    \hline
    scontrol     & show configuration of cluster              \tabularnewline
    \hline
  \end{tabular}
\end{center}

\subsection{Job Control}

Jobs are submitted from the login node and run on 1 or more compute
nodes. Jobs then run until they terminate in some way, e.g.\
normal completion, timeout, abort, end of the world.

\vspace{0.1in}
\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    Command & What it does                                    \tabularnewline
    \hline
    \hline
    srun    & submit interactive job                          \tabularnewline
    \hline
    salloc  & allocate resources for future interactive job   \tabularnewline
    \hline
    sbatch  & submit batch (hands-off) job                    \tabularnewline
    \hline
    scancel & terminate a job                                 \tabularnewline
    \hline
  \end{tabular}
\end{center}

\subsection{Compute Node Access Control}

Interactive access to compute nodes, e.g.\ using \texttt{ssh} after
\texttt{salloc}, is controlled by Secure Shell (SSH) keys, and by
SLURM integration with PAM.

Your account was created with a public/private key-pair already in
place (see directory \texttt{\$HOME/.ssh}).  The content of the public
key \texttt{id\_rsa.pub} is also present in the file
\texttt{authorized\_keys}.

If you create a new key or otherwise change key-related things, you
will need to update

\begin{itemize}
\item the \texttt{authorized\_keys} file on the cluster
\item other systems that use your key-pair for cluster login and
  repository access (e.g.\ github)
\end{itemize}

\splashysays{remember, crill and whale have a common home directory, so
  this one key works for both clusters}

\section{Examples}

\subsection{Partition Configuration}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) scontrol show partitions
PartitionName=crill
   AllocNodes=ALL AllowGroups=users Default=YES
   DefaultTime=NONE DisableRootJobs=NO GraceTime=0 Hidden=NO
   MaxNodes=UNLIMITED MaxTime=04:00:00 MinNodes=1 MaxCPUsPerNode=UNLIMITED
   Nodes=crill-0[01-15]
   Priority=1 RootOnly=NO ReqResv=NO Shared=YES:8 PreemptMode=OFF
   State=UP TotalCPUs=720 TotalNodes=15 SelectTypeParameters=N/A
   DefMemPerNode=UNLIMITED MaxMemPerNode=UNLIMITED

PartitionName=crill-gpu
   AllocNodes=ALL AllowGroups=users Default=NO
   DefaultTime=NONE DisableRootJobs=NO GraceTime=0 Hidden=NO
   MaxNodes=UNLIMITED MaxTime=04:00:00 MinNodes=1 MaxCPUsPerNode=UNLIMITED
   Nodes=crill-102
   Priority=1 RootOnly=NO ReqResv=NO Shared=FORCE:8 PreemptMode=OFF
   State=UP TotalCPUs=24 TotalNodes=1 SelectTypeParameters=N/A
   DefMemPerNode=UNLIMITED MaxMemPerNode=UNLIMITED
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

\subsection{Node Configuration}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) scontrol show node crill-102
NodeName=crill-102 State=DOWN* CPUs=24 AllocCPUs=0 RealMemory=1 TmpDisk=0
   Sockets=2 Cores=12 Threads=1 Weight=1 Features=gpgpu Reason=Not responding [slurm@04/21-14:21:00]
\end{lstlisting}
\vspace{0.1in}

Node ``crill-102'' was down at the time.  It has 24 CPUS (2 sockets x
12-core Opteron) and also has 4 GPUs on board.
\end{minipage}

\subsection{Show Jobs That Are Running/Queued At The Moment}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) squeue
  JOBID PARTITION     NAME     USER  ST       TIME  NODES NODELIST(REASON)
    569     crill km_test_   ******   R    4:02:39      1 crill-014
    576     crill     bash  *******   R    3:41:09      1 crill-001
\end{lstlisting}
\vspace{0.1in}

2 running jobs, each asking for 1 crill node.
\end{minipage}

\subsection{Show Current Partition State}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) sinfo -al
Thu Jan 30 12:09:41 2014
PARTITION  AVAIL  TIMELIMIT   JOB_SIZE ROOT SHARE     GROUPS  NODES       STATE NODELIST
crill*        up    4:00:00 1-infinite   no YES:8      users     14       mixed crill-[001-014]
crill*        up    4:00:00 1-infinite   no YES:8      users      1        idle crill-015
crill-gpu     up    4:00:00 1-infinite   no FORCE      users      1       mixed crill-102
paper         up   infinite 1-infinite   no    NO      paper     13   allocated crill-[001-013]
crill-phys    up   infinite 1-infinite   no YES:8    physics      8   allocated crill-[001-008]
crill-phys    up   infinite 1-infinite   no YES:8    physics      1        idle crill-101
cosc6327      up    2:00:00 1-infinite   no FORCE   cosc6327      1        idle crill-016
\end{lstlisting}
\vspace{0.1in}

Partition listing showing the crill and other partitions and which
nodes are free or used (you may not have access to all partitions).
\end{minipage}

\subsection{Submit A Job}

The most direct way to submit a job is to use SLURM's
\href{https://computing.llnl.gov/linux/slurm/srun.html}{\texttt{srun}}
command.  Another way is to ask for an allocation via the
\href{https://computing.llnl.gov/linux/slurm/salloc.html}{\texttt{salloc}}
command: this is not a job in itself, but rather a container for
running jobs later.

\splashysays{Note that the command-line flags shown in the examples below
  apply to both \texttt{srun} and \texttt{salloc}, as they end up
  doing much the same thing under the hood to launch programs}

\subsubsection{SLURM/PMI Integration}
\label{sss:pmi}

SLURM has a resource management layer called PMI.  This is exposed as
a library, and it is possible to integrate it into other packages so
those packages understand how to talk with SLURM.

You can use \texttt{srun} for both sequential and parallel
(e.g.\ OpenMP and MPI) programs by allocating resources appropriately:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) srun -n 1 ./sequentialhello
Hello world on crill-001
\end{lstlisting}
\end{minipage}
\vspace{0.1in}

The versions of Open-MPI on the clusters all understand how to
interact fully with SLURM.  This means you can (but don't have to) run
Open-MPI programs like this:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) srun -n 4 ./helloworldmpi
Hello from 1 of 4 on crill-001
Hello from 3 of 4 on crill-001
Hello from 0 of 4 on crill-002
Hello from 2 of 4 on crill-002
\end{lstlisting}
\end{minipage}
\vspace{0.1in}

For PGAS programmers, GASNet versions 1.22.0 and newer also understand
PMI.  This means you can (but don't have to) run GASNet-based programs
directly with \texttt{srun}.

\splashysays{We have seen wildly different execution-time behaviors from \texttt{srun} and \texttt{salloc} usage.  This appears to be related to the Open-MPI/GASNet PMI integration.  Caveat emptor.}

\subsubsection{Allocations}

Another way of running jobs in SLURM involves creating an allocation
with the
\href{https://computing.llnl.gov/linux/slurm/salloc.html}{\texttt{salloc}}
command, and then running programs inside that allocation.

\subsubsection{Interactive MPI Programs}
\label{sss:intmpi}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -n 4 mpirun ./helloworld
salloc: Granted job allocation 585
Hello from 1 of 4 on crill-001
Hello from 3 of 4 on crill-001
Hello from 0 of 4 on crill-002
Hello from 2 of 4 on crill-002
salloc: Relinquishing job allocation 585
\end{lstlisting}
\vspace{0.1in}

Allocate 4 processors (which turned out to be from 2 nodes) from the
default crill partition and launch an MPI program.  Note that Open-MPI
on the system has been built with SLURM support, so \texttt{mpirun}
knows how to query SLURM for the number of processors to use (no need
for the \texttt{-np} option, although there is nothing stopping you
from using it if you prefer).
\end{minipage}

\subsubsection{Interactive And Exclusive Login Session}
\label{sec:interactive}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -N 1 --exclusive
tonyc@crill[2](~) squeue
  JOBID PARTITION     NAME     USER  ST       TIME  NODES NODELIST(REASON)
    569     crill km_test_   ******   R   17:28:45      1 crill-014
    577     crill     bash ********   R       8:28      2 crill-[001-002]
    586     crill     bash    tonyc   R       0:03      1 crill-006
\end{lstlisting}
\vspace{0.1in}

Allocate any 1 available node from the crill partition for interactive
use, and do not share it with any other job (as the owner, I can still
log in to that node multiple times, though).  The request will hang if
it can not be satisfied by the current resources (nodes) available,
although various flags can be used to exit immediately or wait for
a specified amount of time before giving up.

\end{minipage}

\splashysays{See section \ref{sec:salloc-usage} for an important point
  about interactive allocations}

\subsubsection{Allocate Specific Node}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -w crill-013
tonyc@crill[2](~) squeue
  JOBID PARTITION     NAME     USER  ST       TIME  NODES NODELIST(REASON)
    569     crill km_test_   ******   R   17:28:45      1 crill-014
    577     crill     bash ********   R       8:28      2 crill-[001-002]
    586     crill     bash    tonyc   R       0:03      1 crill-013
\end{lstlisting}
\vspace{0.1in}

Allocate node crill-013 explicitly.  May be shared with other users
unless it already has been allocated exclusively.  Explicitly
allocating resources comes with pitfalls: if you ask for e.g.\ a
specific node and it is busy, your request will hang, even though many
other resources that could handle your job equally well might be free.

\end{minipage}

\subsubsection{MPI Batch Job}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}[caption={hello.job}]
#!/bin/sh
#SBATCH -n 4
#SBATCH -D /home/tonyc/work/mpi
#SBATCH -o hello.out
#SBATCH --mail-type=END
#SBATCH --mail-user=tonyc@cs.uh.edu

mpirun ./a.out
\end{lstlisting}
\end{minipage}
\vspace{0.1in}

\begin{itemize}
\item Run an Open-MPI program \texttt{./a.out} with 4 processors.
\item Set the working directory for the program run.
\item Put the standard output of the program in the file \texttt{hello.out}.
\item Send me a notification email when the job finishes.
\end{itemize}

\vspace{0.1in}
\begin{minipage}{\linewidth}

The job is submitted with

\begin{lstlisting}
tonyc@crill[1](~) sbatch hello.job
sbatch: Submitted batch job 658
...
<some time later>
...
tonyc@crill[1](~) cat hello.out
Hello from 3 of 4 on crill-001
Hello from 2 of 4 on crill-001
Hello from 1 of 4 on crill-002
Hello from 0 of 4 on crill-002
\end{lstlisting}
\vspace{0.1in}

\end{minipage}

\subsubsection{Discover hostfile}

Although Open-MPI and other environments understand they are running
inside SLURM, some others might not.  So in some cases you might need
to work out which nodes are in an allocation so you can construct a
``hostfile'' to give to a launcher.

The following SLURM command, run inside an allocation, will tell you
which nodes you have:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -N 2
salloc: Granted job allocation 1309
tonyc@crill[2](~) scontrol show hostnames
crill-001
crill-002
tonyc@crill[2](~)
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

and then you can capture this output for later use:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[2](~) scontrol show hostnames > HOSTFILE
tonyc@crill[2](~) cat HOSTFILE
crill-001
crill-002
tonyc@crill[2](~) launcher --hostfile HOSTFILE command
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

\subsection{QDR Infiniband}

In the crill cluster, nodes 1-16 have a QDR Infiniband card in them.
Since these are on a different network than the other Infiniband
cards, there can be problems routing messages in applications that
stripe traffic over multiple cards.

To restrict traffic to just the QDR card, you can e.g.\

\begin{minipage}{\linewidth}
\begin{itemize}
\item tell OpenMPI to ignore the other card
\begin{lstlisting}
tonyc@crill[1](~) export OMPI_MCA_btl_openib_if_exclude=mlx4_0
\end{lstlisting}
\item tell MVAPICH to only use QDR
\begin{lstlisting}
tonyc@crill[1](~) export MV2_IBA_HCA=mlx4_1
\end{lstlisting}
\item tell GASNet to only use QDR
\begin{lstlisting}
tonyc@crill[1](~) export GASNET_IBV_PORTS=mlx4_1
\end{lstlisting}
\end{itemize}
\end{minipage}

% There is also a \texttt{crill-qdr} partition that will allocate only
% these 16 nodes.

\section{What \texttt{salloc} \emph{Doesn't} Do}
\label{sec:salloc-usage}

When you \texttt{salloc} a new allocation, it is tempting to do
something like this to run a sequential or e.g.\ OpenMP application:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc --exclusive ./mycommand
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

or

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc --exclusive -n 8
tonyc@crill[2](~) ./mycommand
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

expecting \texttt{./mycommand} to run on the allocated node.  However,
this is not the case: \texttt{./mycommand} runs on the \emph{login
  node}, which is not what was intended.  This is clear from the shell
prompt in the 2nd example above.

This leads to some confusion, because

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -n 16 mpirun ./myprogram
\end{lstlisting}
\vspace{0.1in}
\end{minipage}

does what you think it will and is correct usage.  Note that
\texttt{mpirun} is actually running on the login node, but the
\texttt{./myprogram}s will run on the allocated nodes because the job
environment creates an MPI hostfile.

In the first case, you can get the name of the allocated node from
\texttt{squeue} and then \texttt{ssh} into that node and run your
application \emph{in situ} or use the SLURM \texttt{srun} command as
discussed in section \ref{sss:pmi}.

\section{Optional Packages}

Apart from the standard packages installed on each node through the
Operating System, there are also many other tools and libraries
available via local installation and customization.  These live under
the directory \texttt{/opt} and can be accessed with the
\texttt{module} command, e.g.\

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) module avail

----------------------------- /usr/share/modules/modulefiles -----------------------------
cray/chapel/1.5                       hpctools/openuh
cuda/4.1                              intel/13.1
cuda/4.2                              login-default
cuda/5.0                              module-info
dot                                   modules
gasnet/mvapich2/gnu/1.20.0/everything mvapich2/1.9
...
hmpp/2.5.1                            slurm
hmpp/3.0.3                            system
hmpp/3.2.4
\end{lstlisting}
\vspace{0.1in}

Here we see all the modules that are currently available (output
truncated for brevity).  You can create your own personal module files
too.  The \texttt{module} command handles setting up your environment
(e.g.\ \texttt{PATH}, \texttt{MANPATH}) when packages are added/loaded
and then will remove settings when you rm/unload.

\end{minipage}

\splashysays{you can use TAB-completion with \texttt{module} to prompt you
  for possible module commands and then names/versions of packages}

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib64/gcc/x86_64-suse-linux/4.5/lto-wrapper
Target: x86_64-suse-linux
Configured with: ../configure --prefix=/usr ...
Thread model: posix
gcc version 4.5.0 20100604 [gcc-4_5-branch revision 160292] (SUSE Linux)

tonyc@crill[1](~) module add gcc/4.4.6

tonyc@crill[1](~) gcc -v
Using built-in specs.
Target: x86_64-unknown-linux-gnu
Configured with: ../configure --prefix=/opt/gnu/gcc/4.4.6 --enable-languages=c,c++,fortran
Thread model: posix
gcc version 4.4.6 (GCC)

tonyc@crill[1](~) module rm gcc/4.4.6

tonyc@crill[1](~) gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib64/gcc/x86_64-suse-linux/4.5/lto-wrapper
Target: x86_64-suse-linux
Configured with: ../configure --prefix=/usr ...
Thread model: posix
gcc version 4.5.0 20100604 [gcc-4_5-branch revision 160292] (SUSE Linux)
\end{lstlisting}
\vspace{0.1in}

Here we switch from the standard OS GCC 4.5.0 to a locally built GCC
version 4.4.6, and then back again (output truncated for brevity).

\end{minipage}

\section{Usage Tips}

\subsection{Shells \& Job Allocations}

When you allocate a job, e.g.\ with \texttt{salloc}, the system spawns
a new shell for that allocation.  It's easy to forget that you're in a
new shell, and that an allocation is still active, especially for an
interactive login job.  So one suggestion is to include the
shell-level in your prompt.  For bash, you could add the shell
variable SHLVL to PS1 in your \texttt{\~{}/.bashrc}:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
PS1="\u@\h[$SHLVL](\w) "
\end{lstlisting}
\end{minipage}

That way, a regular login to crill will be at level 1, but after a
\texttt{salloc}, \$SHLVL will be 2 and will remind you that an
allocation still exists:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) salloc -N 1
salloc: Granted job allocation 599
tonyc@crill[2](~) exit
salloc: Relinquishing job allocation 599
tonyc@crill[1](~)
\end{lstlisting}

\end{minipage}

Bash also provides for inserting escape sequences into the prompt, so
if your terminal emulator supports it, you could make the prompt
change color instead if you like.  See section \ref{sec:references}
for where to find a guide.

\subsection{Temporary Directories}

Each node has 3 temporary directories:

\vspace{0.1in}
\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    Directory & Description\tabularnewline
    \hline
    \hline
    \texttt{/var/tmp}  & persistent, disk-based      \tabularnewline
    \texttt{/tmp}      & not-persistent, disk-based  \tabularnewline
    \texttt{/fast-tmp} & volatile, memory-based      \tabularnewline
    \hline
  \end{tabular}
\end{center}
\vspace{0.1in}

Usually, most programs will use \texttt{/tmp} to write temporary files
(e.g.\ GCC will create intermediate files here).  You can
redirect to e.g.\ \texttt{/fast-tmp} by setting the environment
variable \texttt{TMPDIR} as follows:

\vspace{0.1in}
\begin{minipage}{\linewidth}
\begin{lstlisting}
tonyc@crill[1](~) export TMPDIR=/fast-tmp
\end{lstlisting}
\end{minipage}

Note, though, that \texttt{/fast-tmp} is rather small.  All temporary
directories are subject to cleaning if they fill up.  If you make use
of temporary directories in jobs, it would be appreciated if your
programs cleaned up after themselves (this is generally good practice
anyway, e.g.\ to prevent other people seeing possibly non-public data
when they run their jobs on a node you had earlier).

\section{References}
\label{sec:references}

\begin{enumerate}
\item The SLURM man pages on the crill and whale clusters
\item \href{https://computing.llnl.gov/linux/slurm/documentation.html}
  {https://computing.llnl.gov/linux/slurm/documentation.html}
\item \href{http://www.gnu.org/software/bash/manual/bashref.html\#Printing-a-Prompt}
  {http://www.gnu.org/software/bash/manual/bashref.html\#Printing-a-Prompt}
\item \href{http://modules.sourceforge.net/}
  {http://modules.sourceforge.net/}
\item \href{https://wiki.archlinux.org/index.php/Color\_Bash\_Prompt}
  {https://wiki.archlinux.org/index.php/Color\_Bash\_Prompt}
\end{enumerate}

\section{Acknowledgement}

Development at the University of Houston was supported in part by the
National Science Foundation's Computer Systems Research program under
Award No. CRI-0958464.  Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the authors
and do not necessarily reflect the views of the National Science
Foundation.

\splashysays{Thanks for reading!  If you have comments or suggestions,
  please email the authors.}

\end{document}
